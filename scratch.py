#Group 9

# isnumeric()

# Checks to see if all the characters in a string are numeric.
# Takes string as input
# Returns a boolean value
# Could be used to check a phone number on a website

phone_number =input("Enter your phone number: ")

print(phone_number.isnumeric())
